package com.app.thomas.solitaireplus;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.util.ArrayList;
import java.util.List;

public class ImageHandler {

    private int screenWidth;
    private int screenHeight;
    private Context context;
    private Bitmap background;
    private List<Bitmap> cardDeck;
    private Bitmap acePlaceholder;
    private Bitmap genPlaceholder;

    private int scaledWidth;
    private int scaledHeight;


    public ImageHandler(int screenHeight, int screenWidth, String gameCode, Context context) {
        this.context = context;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        switch (gameCode){
            case "klondike":
                this.scaledWidth = screenWidth / 9;
                this.scaledHeight = (int) (scaledWidth * 1.57);
                this.background = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.mipmap.background), screenWidth, screenHeight, true);
                this.cardDeck = new ArrayList<Bitmap>();
                createDeck(context);
                this.genPlaceholder = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.place_holder), scaledWidth, scaledHeight, true);
                this.acePlaceholder = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ace_place), scaledWidth, scaledHeight, true);
                break;
        }
    }

    private void createDeck(Context context){
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ace_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.two_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.three_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.four_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.five_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.six_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.seven_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.eight_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.nine_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ten_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.jack_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.queen_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.king_hearts), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ace_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.two_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.three_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.four_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.five_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.six_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.seven_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.eight_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.nine_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ten_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.jack_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.queen_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.king_diamonds), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ace_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.two_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.three_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.four_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.five_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.six_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.seven_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.eight_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.nine_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ten_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.jack_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.queen_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.king_spades), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ace_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.two_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.three_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.four_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.five_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.six_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.seven_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.eight_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.nine_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.ten_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.jack_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.queen_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.king_clubs), scaledWidth, scaledHeight, true));
        cardDeck.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.card_back), scaledWidth, scaledHeight, true));
    }

    public Bitmap getBackground() {
        return background;
    }

    public Bitmap getCardImage(int i) {
        return cardDeck.get(i);
    }

    public Bitmap getGenPlaceholder() {
        return genPlaceholder;
    }

    public Bitmap getAcePlaceholder() {
        return acePlaceholder;
    }

    public int getCardWidth() {
        return scaledWidth;
    }

    public int getCarddHeight() {
        return scaledHeight;
    }
}
