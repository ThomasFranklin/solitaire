package com.app.thomas.solitaireplus;


import android.graphics.Canvas;

import java.util.LinkedList;
import java.util.List;

public class MovementAnimator {

    private static final double ANIMATION_TIME = 80.0;
    private static final double ANIMATION_INTERVAL = 10.0;

    private long startTime;
    private long frameStartTime;

    private boolean animationInProgress;

    private List<Card> animatedCards;
    private List<Point> targetPosns;

    private Point targetPoint;

    private double deltaX;
    private double deltaY;

    private String targetLocation;
    private int targetStackIndex;

    public MovementAnimator() {
        this.animationInProgress = false;
        this.animatedCards = new LinkedList<>();
        this.targetPosns = new LinkedList<>();
    }

    public void startAnimation(String targetLocation, int targetStackIndex, KlondikeGameBoard board){
        animationInProgress = true;
        this.targetLocation = targetLocation;
        this.targetStackIndex = targetStackIndex;
        this.targetPoint = targetPosns.get(0);
        Card firstCard = animatedCards.get(0);
        deltaX = (targetPoint.x - firstCard.getXPos()) / ANIMATION_INTERVAL;
        deltaY = (targetPoint.y - firstCard.getYPos()) / ANIMATION_INTERVAL;
        this.startTime = System.currentTimeMillis();
        System.out.println("Animation Started");
        while (animationInProgress){
            updateAnimation(System.currentTimeMillis(), board);
        }
    }

    public void updateAnimation(long curTime, KlondikeGameBoard board){
        if ((curTime - startTime) >= ANIMATION_TIME){
            System.out.println("Finishing Animation");
            // Finish animation
            for (int i = 0 ; i < animatedCards.size() ; i++){
                Card card = animatedCards.get(i);
                card.setPosition(targetPosns.get(i));
            }
            board.placeCardsAfterAnimation(animatedCards, targetLocation, targetStackIndex);
            // Reset animation
            animationInProgress = false;
            animatedCards.clear();
            targetPosns.clear();
            System.out.println("Animation finished");
        }
        else if ((curTime - frameStartTime >= ANIMATION_INTERVAL)){
            System.out.println("Updating animation. Cards: " + animatedCards.size());
            //Update the positions
            for (Card card : animatedCards){
                Point newPos = new Point((int)(card.getXPos() + deltaX), (int)(card.getYPos() + deltaY));
                card.setPosition(newPos);
                System.out.println("" + newPos.x + ", " + newPos.y);
            }
            //Update frame start time
            frameStartTime = curTime;
        }
    }

    public void drawCards(Canvas canvas, ImageHandler images){
        for (int i = 0 ; i < animatedCards.size() ; i++){
            Card card = animatedCards.get(i);
            canvas.drawBitmap(images.getCardImage(card.getImdex()), card.getXPos(), card.getYPos(), null);
        }
    }

    public void setAnimatedCards(List<Card> animatedCards) {
        for (Card card: animatedCards){
            this.animatedCards.add(card);
        }
        System.out.println("Cards animated: " + this.animatedCards.size());
    }

    public void addAnimatedCard(Card card){
        animatedCards.add(card);
    }

    public void setTargetPosns(List<Point> targetPosns) {
        for (Point point: targetPosns){
            this.targetPosns.add(point);
        }
    }

    public void addTargetPosn(Point point){
        targetPosns.add(point);
    }

    public boolean isAnimationInProgress() {
        return animationInProgress;
    }
}
