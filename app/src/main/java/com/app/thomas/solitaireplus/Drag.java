package com.app.thomas.solitaireplus;


import android.graphics.Canvas;
import java.util.LinkedList;
import java.util.List;

public class Drag {

    private boolean possibleDrag;
    private boolean dragInProgress;
    private List<Card> dragCards;
    private List<Point> cardsInitPosns;
    private String oLocation;
    private int oStackIndex;
    private int oIndex;
    private long dragStartTime;

    public Drag() {
        this.possibleDrag = false;
        this.dragInProgress = false;
        this.dragCards = new LinkedList<>();
        this.cardsInitPosns = new LinkedList<>();
    }

    public boolean isPossibleDrag() {
        return possibleDrag;
    }

    public void setPossibleDrag(boolean possibleDrag) {
        this.possibleDrag = possibleDrag;
    }

    public boolean isDragInProgress() {
        return dragInProgress;
    }

    public void setDragInProgress(boolean dragInProgress) {
        this.dragInProgress = dragInProgress;
    }

    public void addCardToDrag(Card card, Point pos){
        dragCards.add(card);
        cardsInitPosns.add(pos);
    }

    public List<Point> getCardsInitPosns() {
        return cardsInitPosns;
    }

    public List<Card> getDragCards() {
        return dragCards;
    }

    public int getoIndex() {
        return oIndex;
    }

    public void setoIndex(int oIndex) {
        this.oIndex = oIndex;
    }

    public String getoLocation() {
        return oLocation;
    }

    public void setoLocation(String oLocation) {
        this.oLocation = oLocation;
    }

    public int getoStackIndex() {
        return oStackIndex;
    }

    public void setoStackIndex(int oStackIndex) {
        this.oStackIndex = oStackIndex;
    }

    public long getDragStartTime() {
        return dragStartTime;
    }

    public void setDragStartTime(long dragStartTime) {
        this.dragStartTime = dragStartTime;
    }

    public void resetDrag(){
        possibleDrag = false;
        dragInProgress = false;
        dragCards.clear();
        cardsInitPosns.clear();
        oLocation = null;
    }

    public void drawDragCards(Canvas canvas, ImageHandler images) {
        for (int i = 0; i < dragCards.size(); i++){
            Card card = dragCards.get(i);
            canvas.drawBitmap(images.getCardImage(card.getImdex()), card.getXPos(), card.getYPos(), null);
        }
    }
}
