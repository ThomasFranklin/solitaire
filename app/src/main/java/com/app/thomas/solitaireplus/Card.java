package com.app.thomas.solitaireplus;

import java.io.Serializable;

public class Card implements Serializable{

    private int number;
    private String suit;
    private String color;
    private int imdex;
    private boolean faceUp;
    private Point position;
    private int cardWidth;
    private int cardHeight;

    public Card(String color, int number, String suit, int cardWidth, int cardHeight) {
        this.color = color;
        this.faceUp = false;
        this.number = number;
        this.suit = suit;
        this.position = new Point(0, 0);
        this.cardWidth = cardWidth;
        this.cardHeight = cardHeight;
    }

    public Card(int cardHeight, int cardWidth, String color, boolean faceUp, int imdex, int number, Point position, String suit) {
        this.cardHeight = cardHeight;
        this.cardWidth = cardWidth;
        this.color = color;
        this.faceUp = faceUp;
        this.imdex = imdex;
        this.number = number;
        this.position = position;
        this.suit = suit;
    }

    public int getXPos(){
        return position.x;
    }

    public int getYPos(){
        return position.y;
    }


    public void setPosition(Point position) {
        this.position = position;
    }

    public int getNumber() {
        return number;
    }

    public String getColor() {
        return color;
    }

    public String getSuit() {
        return suit;
    }

    public boolean isFaceUp(){
        return faceUp;
    }

    public int getImdex() {
        return imdex;
    }

    public void setImdex(int imdex) {
        this.imdex = imdex;
    }

    public void setFaceUp(boolean faceUp) {
        this.faceUp = faceUp;
    }

    public boolean isWithinBounds(Point pt){
        return ((pt.x >= position.x)&&(pt.x <= (position.x + cardWidth))&&
                (pt.y >= position.y)&&(pt.y <= (position.y + cardHeight)));
    }

    public Card cloneCard(){
        return new Card(this.cardHeight, this.cardWidth, this.color, this.faceUp, this.imdex,
                this.number, new Point(this.getXPos(), this.getYPos()), this.suit);
    }
}
