package com.app.thomas.solitaireplus;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

public class StatsHandler{

    private static final String FILE_NAME = "klondike_stats";

    public int numberOfGames;
    public int numberOfWins;
    public float winPercentage;
    public int totalTime;
    public int averageGameTime;
    public int bestGameTime;
    public int totalMoves;
    public int averageMoves;
    public int bestMoves;
    public int currentWinStreak;
    public int bestWinStreak;

    public StatsHandler() {
        this.averageGameTime = 0;
        this.averageMoves = 0;
        this.bestGameTime = 0;
        this.bestMoves = 0;
        this.bestWinStreak = 0;
        this.currentWinStreak = 0;
        this.numberOfGames = 0;
        this.numberOfWins = 0;
        this.totalMoves = 0;
        this.totalTime = 0;
        this.winPercentage = 0;
    }

    public void writeToFile(Context context, KlondikeGameStats gameStats){
        LinkedList<KlondikeGameStats> stats = readFile(context);
        stats.add(gameStats);
        File file = new File(context.getFilesDir(), FILE_NAME);
        try {
            FileOutputStream statsOut = new FileOutputStream(file);
            ObjectOutputStream statsListOut = new ObjectOutputStream(statsOut);
            statsListOut.writeObject(stats);
            statsOut.close();
            statsListOut.close();
        }
        catch (IOException e){
            System.out.println("Stats can't be written to the file. Time for hours of debugging!");
        }
    }

    private LinkedList<KlondikeGameStats> readFile(Context context){
        LinkedList<KlondikeGameStats> statsList;
        File file = context.getFileStreamPath(FILE_NAME);
        if (file.exists()){
            try {
                FileInputStream statsIn = new FileInputStream(file);
                ObjectInputStream statsListIn = new ObjectInputStream(statsIn);
                statsList = (LinkedList<KlondikeGameStats>)statsListIn.readObject();
                statsListIn.close();
                statsIn.close();
            }
            catch (IOException e){
                e.printStackTrace();
                System.out.println("File could not be read for some reason. Proceed to blame java.");
                statsList = new LinkedList<>();
            }
            catch (ClassNotFoundException e){
                System.out.println("Java isn't happy with the contents of this file, bro.");
                statsList = new LinkedList<>();
            }
        }
        else {
            statsList = new LinkedList<>();
        }
        return statsList;
    }

    private void summonStats(Context context){
        //Summons statistics from the depths of Solitaire's cache
        //Each game's stats are added to this pretty little list, which gets longer the more games one plays
        //There is a potential for this list to get so long that it crashes the app...
        //But at that point you need to stop playing solitaireplus and get a life anyway, so we'll say it's intentional
        clearStatValues();
        LinkedList<KlondikeGameStats> stats = readFile(context);
        if (stats.size() > 0){
            for (int i = 0 ; i < stats.size() ; i++){
                KlondikeGameStats gameStats = stats.get(i);
                numberOfGames++;
                if (gameStats.isGameWon()){
                    numberOfWins++;
                    currentWinStreak++;
                    bestWinStreak = Math.max(bestWinStreak, currentWinStreak);
                }
                else{
                    bestWinStreak = Math.max(bestWinStreak, currentWinStreak);
                    currentWinStreak = 0;
                }
                totalMoves = totalMoves + gameStats.getMoves();
                totalTime = totalTime + gameStats.getTime();
                if ((gameStats.getMoves() < bestMoves)||(bestMoves == 0)){
                    bestMoves = gameStats.getMoves();
                }
                if ((gameStats.getTime() < bestGameTime)||(bestGameTime == 0)){
                    bestGameTime = gameStats.getTime();
                }
            }
            if (numberOfGames > 0){
                winPercentage = round((((float) numberOfWins) / ((float) numberOfGames)) * 100);
                averageGameTime = totalTime / numberOfGames;
                averageMoves = totalMoves / numberOfGames;
            }
        }
        System.out.println("Number of games: " + numberOfGames);
        System.out.println("Number of wins: " + numberOfWins);
        System.out.println("Win percentage: " + winPercentage + "%");
        System.out.println("Total time: " + totalTime);
        System.out.println("Average game time: " + averageGameTime);
        System.out.println("Best game time: " + bestGameTime);
        System.out.println("Total moves: " + totalMoves);
        System.out.println("Average moves: " + averageMoves);
        System.out.println("Best moves: " + bestMoves);
        System.out.println("Current win streak: " + currentWinStreak);
        System.out.println("Best win streak: " + bestWinStreak);
    }

    private float round(float value){
        return (float) (Math.round(value * 10.0) / 10.0);
    }

    public void compileStatsScreen(View statsScreen, Context context){
        summonStats(context);
        TextView totalGamesData = (TextView) statsScreen.findViewById(R.id.totalGamesData);
        totalGamesData.setText("" + numberOfGames);
        TextView totalWinsData = (TextView) statsScreen.findViewById(R.id.winsData);
        totalWinsData.setText("" + numberOfWins);
        TextView winPercentageData = (TextView) statsScreen.findViewById(R.id.winPercentageData);
        winPercentageData.setText("" + winPercentage + "%");
        TextView currentWinStreakData = (TextView) statsScreen.findViewById(R.id.currentWinStreakData);
        currentWinStreakData.setText("" + currentWinStreak);
        TextView bestWinStreakData = (TextView) statsScreen.findViewById(R.id.bestWinStreakData);
        bestWinStreakData.setText("" + bestWinStreak);
        TextView totalMovesData = (TextView) statsScreen.findViewById(R.id.totalMovesData);
        totalMovesData.setText("" + totalMoves);
        TextView averageMovesData = (TextView) statsScreen.findViewById(R.id.averageMovesData);
        averageMovesData.setText("" + averageMoves);
        TextView bestMovesData = (TextView) statsScreen.findViewById(R.id.bestMovesData);
        bestMovesData.setText("" + bestMoves);
        TextView totalTimeData = (TextView) statsScreen.findViewById(R.id.totalTimeData);
        totalTimeData.setText(formatTime(totalTime));
        TextView averageTimeData = (TextView) statsScreen.findViewById(R.id.averageGameTimeData);
        averageTimeData.setText(formatTime(averageGameTime));
        TextView bestTimeData = (TextView) statsScreen.findViewById(R.id.bestTimeData);
        bestTimeData.setText(formatTime(bestGameTime));
    }

    public void compileWinScreen(View winScreen, Context context, KlondikeGameStats stats){
        summonStats(context);
        TextView gameTimeData = (TextView) winScreen.findViewById(R.id.winScreenGameTimeData);
        gameTimeData.setText("" + formatTime(stats.getTime()));
        TextView gameMovesData = (TextView) winScreen.findViewById(R.id.winScreenMovesData);
        gameMovesData.setText("" + stats.getMoves());
        TextView winStreakData = (TextView) winScreen.findViewById(R.id.winScreenWinStreakData);
        winStreakData.setText("" + currentWinStreak);
        TextView bestWinStreakData = (TextView) winScreen.findViewById(R.id.winScreenBestWinStreakData);
        bestWinStreakData.setText("" + bestWinStreak);
        TextView winPercentageData = (TextView) winScreen.findViewById(R.id.winScreenWinPercentageData);
        winPercentageData.setText("" + winPercentage + "%");
    }

    private String formatTime(long rawSeconds){
        int formattedSeconds = (int)(rawSeconds % 60);
        int rawMinutes = (int)Math.floor(rawSeconds / 60.0);
        int formattedMinutes = (rawMinutes % 60);
        int rawHours = (int)Math.floor(rawMinutes / 60.0);
        String seconds = "" + formattedSeconds;
        String minutes = "" + formattedMinutes;
        String hours = "" + rawHours;
        if (formattedSeconds < 10){
            seconds = "0" + seconds;
        }
        if (formattedMinutes < 10){
            minutes = "0" + minutes;
        }
        return ("" + hours + ":" + minutes + ":" + seconds);
    }

    public void resetStatistics(Context context){
        deleteFile(context);
        clearStatValues();
    }

    private void clearStatValues(){
        this.averageGameTime = 0;
        this.averageMoves = 0;
        this.bestGameTime = 0;
        this.bestMoves = 0;
        this.bestWinStreak = 0;
        this.currentWinStreak = 0;
        this.numberOfGames = 0;
        this.numberOfWins = 0;
        this.totalMoves = 0;
        this.totalTime = 0;
        this.winPercentage = 0;
    }

    private void deleteFile(Context context){
        File file = context.getFileStreamPath(FILE_NAME);
        if (file.exists()){
            file.delete();
        }
    }
}
