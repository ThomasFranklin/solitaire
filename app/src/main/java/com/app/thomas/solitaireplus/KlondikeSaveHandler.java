package com.app.thomas.solitaireplus;


import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

public class KlondikeSaveHandler {

    private static final String GAME_BOARD_FILENAME = "saved_gamestate_board";
    private static final String SAVED_STATS_FILENAME = "saved_gamestate_stats";

    public static void writeStateToFile(Context context, KlondikeGameStats stats, KlondikeGameBoard gameBoard){
        //It's LinkedList-ception
        LinkedList<LinkedList<LinkedList<Card>>> board = gameBoard.saveBoardState(new LinkedList<LinkedList<LinkedList<Card>>>());
        File boardFile = new File(context.getFilesDir(), GAME_BOARD_FILENAME);
        File statsFile = new File(context.getFilesDir(), SAVED_STATS_FILENAME);
        try{
            FileOutputStream fOut = new FileOutputStream(boardFile);
            ObjectOutputStream oOut = new ObjectOutputStream(fOut);
            oOut.writeObject(board);
            oOut.close();
            fOut.close();

        }
        catch (IOException e){
            e.printStackTrace();
            System.out.println("Savedstate board file could not be written");
        }
        try {
            FileOutputStream fOut = new FileOutputStream(statsFile);
            ObjectOutputStream oOut = new ObjectOutputStream(fOut);
            oOut.writeObject(stats);
            oOut.close();
            fOut.close();
        }
        catch (IOException e){
            e.printStackTrace();
            System.out.println("Savedstate stats file could not be written");
        }
    }

    public static boolean checkIfSavedStateExists(Context context){
        File file = new File(context.getFilesDir(), GAME_BOARD_FILENAME);
        return file.exists();
    }

    public static void deleteSavedGame(Context context){
        if (checkIfSavedStateExists(context)){
            File boardFile = context.getFileStreamPath(GAME_BOARD_FILENAME);
            File statsFile = context.getFileStreamPath(SAVED_STATS_FILENAME);
            boardFile.delete();
            statsFile.delete();
        }
    }

    public static KlondikeGameStats getSavedGameStats(Context context) {
        KlondikeGameStats stats = null;
        File statsFile = context.getFileStreamPath(SAVED_STATS_FILENAME);
        try {
            FileInputStream fIn = new FileInputStream(statsFile);
            ObjectInputStream oIn = new ObjectInputStream(fIn);
            stats = (KlondikeGameStats) oIn.readObject();
            oIn.close();
            fIn.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Savedstate stats file could not be read");
        } catch (ClassNotFoundException e) {
            System.out.println("Java is angry for some reason, you need to debug");

        }
        return stats;
    }

    public static KlondikeGameStats restoreSavedState(Context context, KlondikeGameBoard gameBoard){
        File boardFile = context.getFileStreamPath(GAME_BOARD_FILENAME);
        File statsFile = context.getFileStreamPath(SAVED_STATS_FILENAME);
        LinkedList<LinkedList<LinkedList<Card>>> board = new  LinkedList<>();
        KlondikeGameStats stats = new KlondikeGameStats(System.currentTimeMillis());
        try{
            FileInputStream fIn = new FileInputStream(boardFile);
            ObjectInputStream oIn = new ObjectInputStream(fIn);
            board = (LinkedList<LinkedList<LinkedList<Card>>>) oIn.readObject();
            oIn.close();
            fIn.close();
        }
        catch (IOException e){
            e.printStackTrace();
            System.out.println("Savedstate board file could not be read");
        }
        catch (ClassNotFoundException e){
            System.out.println("Java is angry for some reason, you need to debug");
        }
        try{
            FileInputStream fIn = new FileInputStream(statsFile);
            ObjectInputStream oIn = new ObjectInputStream(fIn);
            stats = (KlondikeGameStats) oIn.readObject();
            oIn.close();
            fIn.close();
        }
        catch (IOException e){
            e.printStackTrace();
            System.out.println("Savedstate stats file could not be read");
        }
        catch (ClassNotFoundException e){
            System.out.println("Java is angry for some reason, you need to debug");
        }
        boardFile.delete();
        statsFile.delete();
        gameBoard.restoreBoard(board);
        return stats;
    }
}
