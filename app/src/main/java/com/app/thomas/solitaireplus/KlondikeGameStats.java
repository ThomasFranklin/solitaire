package com.app.thomas.solitaireplus;


import java.io.Serializable;

//Statistics for an individual game
public class KlondikeGameStats implements Serializable{

    //Since users may close app and finish later, game time for every time the user closes the app are added here
    private long startTime;

    private boolean gameWon;
    private int moves;
    private int time;

    public KlondikeGameStats(long startTime) {
        this.startTime = startTime;
        this.gameWon = false;
        this.moves = 0;
        this.time = 0;
    }

    public void setGameWon(boolean gameWon) {
        this.gameWon = gameWon;
    }

    public void addTime(long rawTime){
        System.out.println("Time added to stat file");
        int modifiedTime = (int)((rawTime - startTime) / 1000.0);
        time = time + modifiedTime;
    }

    public void modifiyStartTime(long newTime){
        startTime = newTime;
    }

    public void addMove(){
        moves++;
    }

    public boolean isGameWon() {
        return gameWon;
    }

    public int getMoves() {
        return moves;
    }

    public int getTime() {
        return time;
    }
}
