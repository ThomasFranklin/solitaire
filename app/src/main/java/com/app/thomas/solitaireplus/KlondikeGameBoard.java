package com.app.thomas.solitaireplus;

import android.graphics.Canvas;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class KlondikeGameBoard {

    private LinkedList<Card> drawStack;
    private LinkedList<Card> activeStack;
    private LinkedList<Card> wasteStack;
    private LinkedList<LinkedList<Card>> aceStacks;
    private LinkedList<LinkedList<Card>> board;
    private List<Point> aceStacksPosn;
    private List<Point> placeHolderPosn;
    private List<Point> activeStackPosn;
    private ImageHandler images;

    private int screenWidth;
    private int screenHeight;
    private int cardWidth;
    private int cardHeight;
    private boolean activeStackCleared;

    private static final int DRAW_STACK_POSN = 7;

    public KlondikeGameBoard(ImageHandler images, int screenWidth, int screenHeight) {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.cardWidth = images.getCardWidth();
        this.cardHeight = images.getCarddHeight();
        this.images = images;
        this.drawStack = new LinkedList<>();
        this.activeStack = new LinkedList<>();
        this.wasteStack = new LinkedList<>();
        this.aceStacks = new LinkedList<>();
        for (int i = 0 ; i < 4 ; i++){
            aceStacks.add(new LinkedList<Card>());
        }
        this.board = new LinkedList<>();
        for (int i = 0 ; i < 7 ; i++){
            board.add(new LinkedList<Card>());
        }
        this.aceStacksPosn = new ArrayList<>();
        this.placeHolderPosn = new ArrayList<>();
        this.activeStackPosn = new ArrayList<>();
        this.activeStackCleared = false;
    }

    public void initialize(){
        List<Card> deck = mapImages(makeDeck());
        dealDeck(deck);
        positionCards();
        positionPlaceHolders();
    }

    private List<Card> makeDeck(){
        ArrayList<String> listOfSuits = new ArrayList<>();
        listOfSuits.add("Heart");
        listOfSuits.add("Diamond");
        listOfSuits.add("Spade");
        listOfSuits.add("Club");
        List<Card> deck = new LinkedList<>();
        for(int i = 0 ; i < 4 ; i++ ){
            for (int x = 1 ; x < 14 ; x++){
                Card card;
                if ((listOfSuits.get(i).equals("Spade"))||(listOfSuits.get(i).equals("Club"))){
                    card = new Card("black", x, listOfSuits.get(i), cardWidth, cardHeight);
                }
                else {
                    card = new Card("red", x, listOfSuits.get(i), cardWidth, cardHeight);
                }
                deck.add(card);
            }
        }
        return deck;
    }

    private List<Card> mapImages(List<Card> deck){
        for (int i = 51 ; i >= 0 ; i--){
            deck.get(i).setImdex(i);
        }
        return deck;
    }

    private void dealDeck(List<Card> deck){
        Random random = new Random();
        do {
            int iDeck =  random.nextInt(deck.size());
            Card cardToBePlaced = deck.get(iDeck);
            for (int i = 0 ; i < board.size() ; i++){
                List<Card> pile = board.get(i);
                if ((pile.size() == i + 1)&&(i == 6)){
                    drawStack.add(cardToBePlaced);
                    break;
                }
                if (pile.size() < i + 1){
                    pile.add(cardToBePlaced);
                    break;
                }
            }
            deck.remove(iDeck);
        }
        while(deck.size() > 0);
    }

    private void positionCards(){
        for (int iBoard = 0; iBoard < board.size(); iBoard++) {
            List<Card> pile = board.get(iBoard);
            for (int iPile = 0 ; iPile < pile.size() ; iPile++) {
                Card card = pile.get(iPile);
                int x = (int) (((iBoard + 1.0) * (screenWidth / 8.0)) - (cardWidth / 2.0));
                int y = (int) (((screenHeight / 4.0)) + (iPile * (cardHeight / 5.0)));
                card.setPosition(new Point(x, y));
                if (iPile == (pile.size() - 1)) {
                    card.setFaceUp(true);
                }
            }
        }
        for (int i=0 ; i < drawStack.size() ; i++){
            Card card = drawStack.get(i);
            int x = (int) ((1.0 / 8.0) * screenWidth) - (cardWidth / 2);
            int y = (int) (screenHeight / 16.0);
            card.setPosition(new Point(x, y));
        }
    }

    private void positionPlaceHolders(){
        for (int aceStack = 0 ; aceStack < 4 ; aceStack++){
            int x = (int) (((aceStack + 4.0) * screenWidth / 8.0) - (cardWidth / 2.0));
            int y = (screenHeight / 16);
            aceStacksPosn.add(new Point(x, y));
        }
        for (int iActiveStack = 0; iActiveStack < 3 ; iActiveStack++) {
            int x = (int) (screenWidth * (4.5 / 16.0) - ((2.0 - iActiveStack) * (cardWidth / 4.0)));
            int y = (int) (screenHeight / 16.0);
            activeStackPosn.add(new Point(x, y));
        }
        for (int iBoard = 0 ; iBoard < 7 ; iBoard++){
            int x = (int) (((iBoard + 1.0) * (screenWidth / 8.0)) - (cardWidth / 2.0));
            int y = (screenHeight / 4);
            placeHolderPosn.add(new Point(x, y));
        }
        int drawStackX = (int) ((1.0 / 8.0) * screenWidth) - (cardWidth / 2);
        int drawStackY = (int) (screenHeight / 16.0);
        placeHolderPosn.add(new Point(drawStackX, drawStackY));
    }

    //Used to check if game is won
    public boolean checkIfAceStacksFull(){
        for (List<Card> stack: aceStacks){
            if (stack.size() < 13){
                return false;
            }
        }
        return true;
    }




    //Drag initializer functions
    public void handleInitialDrag(Point pt, Drag drag){
        //Check if activeStack is in bounds
        if ((activeStack.size() > 0)&&(activeStack.get(0).isWithinBounds(pt))){
            System.out.println("Active stack drag possible");
            Card card = activeStack.get(0);
            Point oPosn = new Point(card.getXPos(), card.getYPos());
            drag.setPossibleDrag(true);
            drag.setDragStartTime(System.currentTimeMillis());
            drag.addCardToDrag(card, oPosn);
            drag.setoLocation("activeStack");
            drag.setoStackIndex(0);
            drag.setoIndex(0);
            return;
        }
        //Check if aceStacks are in bounds
        for (int i = 0 ; i < aceStacks.size() ; i++){
            List<Card> stack = aceStacks.get(i);
            if ((stack.size() > 0)&&(stack.get(stack.size() - 1).isWithinBounds(pt))){
                System.out.println("Ace stack drag possible");
                Card card = stack.get(stack.size() - 1);
                Point oPosn = new Point(card.getXPos(), card.getYPos());
                drag.setPossibleDrag(true);
                drag.setDragStartTime(System.currentTimeMillis());
                drag.addCardToDrag(card, oPosn);
                drag.setoLocation("aceStack");
                drag.setoStackIndex(i);
                drag.setoIndex(stack.size() - 1);
                return;
            }
        }
        //Check if board is in bounds
        for (int i = 0 ; i < board.size() ; i++){
            List<Card> stack = board.get(i);
            for (int j = stack.size() - 1 ; j >= 0 ; j--){
                if ((stack.size() > 0)&&(stack.get(j).isFaceUp())&&(stack.get(j).isWithinBounds(pt))){
                    System.out.println("Board drag possible");
                    for (int dCardsIndex = j ; dCardsIndex < stack.size() ; dCardsIndex++){
                        Card card = stack.get(dCardsIndex);
                        Point oPosn = new Point(card.getXPos(), card.getYPos());
                        drag.addCardToDrag(card, oPosn);
                    }
                    drag.setPossibleDrag(true);
                    drag.setDragStartTime(System.currentTimeMillis());
                    drag.setoLocation("board");
                    drag.setoStackIndex(i);
                    drag.setoIndex(j);
                    return;
                }
            }
        }
        System.out.println("No drag possible");
    }



    //Handles Drag movement
    public void handleDragMove(Point pt, Drag drag){
        long curTime = System.currentTimeMillis();
        if ((drag.isPossibleDrag())&&(drag.getDragCards().size() > 0)&&(curTime - drag.getDragStartTime() >= 100)){
            final int MOVE_DELTA_X = (int)(cardWidth / 2.0);
            final int MOVE_DELTA_Y = (int)(cardHeight / 4.0);
            final int STACK_DELTA_Y = (int)(cardHeight/ 5.0);
            if (drag.getoLocation().equals("activeStack")) {
                if (!drag.isDragInProgress()){
                    activeStack.remove(drag.getoIndex());
                    drag.setDragInProgress(true);
                }
                int newX = pt.x - MOVE_DELTA_X;
                int newY = pt.y - MOVE_DELTA_Y;
                Point newPos = new Point(newX, newY);
                drag.getDragCards().get(0).setPosition(newPos);
                return;
            }
            else if (drag.getoLocation().equals("aceStack")) {
                if (!drag.isDragInProgress()){
                    aceStacks.get(drag.getoStackIndex()).remove(drag.getoIndex());
                    drag.setDragInProgress(true);
                }
                int newX = pt.x - MOVE_DELTA_X;
                int newY = pt.y - MOVE_DELTA_Y;
                Point newPos = new Point(newX, newY);
                drag.getDragCards().get(0).setPosition(newPos);
                return;
            }
            else if (drag.getoLocation().equals("board")) {
                if (!drag.isDragInProgress()){
                    List<Card> stack = board.get(drag.getoStackIndex());
                    for (int i = stack.size() - 1 ; i >= drag.getoIndex() ; i--){
                        stack.remove(i);
                    }

                    drag.setDragInProgress(true);
                }
                for (int i = 0; i < drag.getDragCards().size(); i++) {
                    int newX = pt.x - MOVE_DELTA_X;
                    int newY = pt.y - MOVE_DELTA_Y + (i * STACK_DELTA_Y);
                    Point newPos = new Point(newX, newY);
                    drag.getDragCards().get(i).setPosition(newPos);

                }
                return;
            }
            System.out.println("Corrupted Drag");
        }
    }




    //Drag release methods
    public void handleDragRelease(Point release, Drag drag, KlondikeGameStats stats, KlondikeGameView view, MovementAnimator movementAnimator, UndoHandler undoHandler){
        //Places Cards on Ace Stack
        if ((drag.getDragCards().size() == 1)&&(release.y < placeHolderPosn.get(0).y)){
            Card dragCard = drag.getDragCards().get(0);
            for (int i = 0 ; i < aceStacks.size() ; i++){
                List<Card> stack = aceStacks.get(i);
                if ((stack.size() == 0)&&(dragCard.getNumber() == 1)&&
                        (isWithinBounds(aceStacksPosn.get(i), release))){
                    dragCard.setPosition(aceStacksPosn.get(i));
                    stack.add(dragCard);
                    if ((drag.getoLocation().equals("board"))&&(board.get(drag.getoStackIndex()).size() > 0)) {
                        board.get(drag.getoStackIndex()).get(drag.getoIndex() - 1).setFaceUp(true);
                    }
                    activeStackOutOfCards();
                    stats.addMove();
                    //undoHandler.createUndoState(drag.getDragCards(), drag.getCardsInitPosns(),
                            //"aceStack", i, stack.size() - 1, drag.getoLocation(), drag.getoStackIndex(), activeStackCleared);
                    return;
                }
                else if ((stack.size() > 0)&&((stack.size() + 1) == dragCard.getNumber())&&
                        (stack.get(stack.size() - 1).getSuit().equals(dragCard.getSuit()))&&
                        (isWithinBounds(aceStacksPosn.get(i), release))){
                    dragCard.setPosition(aceStacksPosn.get(i));
                    stack.add(dragCard);
                    if ((drag.getoLocation().equals("board"))&&(board.get(drag.getoStackIndex()).size() > 0)) {
                        board.get(drag.getoStackIndex()).get(drag.getoIndex() - 1).setFaceUp(true);
                    }
                    activeStackOutOfCards();
                    stats.addMove();
                    view.checkIfWon();
                    //undoHandler.createUndoState(drag.getDragCards(), drag.getCardsInitPosns(),
                            //"aceStack", i, stack.size() - 1, drag.getoLocation(), drag.getoStackIndex(), activeStackCleared);
                    return;
                }
            }
        }
        //Places cards on board
        for (int i = 0 ; i < board.size() ; i++){
            List<Card> stack = board.get(i);
            if ((stack.size() == 0)&&(drag.getDragCards().get(0).getNumber() == 13)&&
                    (isWithinBounds(placeHolderPosn.get(i), release))){
                for (Card card: drag.getDragCards()){
                    card.setFaceUp(true);
                    stack.add(card);
                }
                rePositionCards(i);
                if ((drag.getoLocation().equals("board"))&&(drag.getoIndex() != 0)) {
                    board.get(drag.getoStackIndex()).get(drag.getoIndex() - 1).setFaceUp(true);
                }
                activeStackOutOfCards();
                stats.addMove();
                System.out.println("Initial Position Size: " + drag.getCardsInitPosns().size());
                //undoHandler.createUndoState(drag.getDragCards(), drag.getCardsInitPosns(),
                        //"board", i, stack.size() - 1, drag.getoLocation(), drag.getoStackIndex(), activeStackCleared);
                return;
            }
            else if ((stack.size() > 0)&&(stack.get(stack.size() - 1).isWithinBounds(release))&&
                    (cardCanBePlaced(drag.getDragCards().get(0), stack.get(stack.size() - 1)))){
                for (Card card: drag.getDragCards()){
                    card.setFaceUp(true);
                    stack.add(card);
                }
                rePositionCards(i);
                if ((drag.getoLocation().equals("board"))&&(board.get(drag.getoStackIndex()).size() > 0)) {
                    board.get(drag.getoStackIndex()).get(drag.getoIndex() - 1).setFaceUp(true);
                }
                activeStackOutOfCards();
                stats.addMove();
                System.out.println("Initial Position Size: " + drag.getCardsInitPosns().size());
                //undoHandler.createUndoState(drag.getDragCards(), drag.getCardsInitPosns(),
                        //"board", i, stack.size() - 1, drag.getoLocation(), drag.getoStackIndex(), activeStackCleared);
                return;
            }
        }
        //Returns cards to initial position if drag not valid
        movementAnimator.setAnimatedCards(drag.getDragCards());
        movementAnimator.setTargetPosns(drag.getCardsInitPosns());
        String location = drag.getoLocation();
        int stackIndex = drag.getoStackIndex();
        drag.resetDrag();
        movementAnimator.startAnimation(location, stackIndex, this);
    }

    private boolean isWithinBounds(Point upperBound, Point check){
        return ((check.x >= upperBound.x)&&(check.x <= (upperBound.x + cardWidth))&&
                (check.y >= upperBound.y)&&(check.y <= (upperBound.y + cardHeight)));
    }

    private boolean cardCanBePlaced(Card dragCard, Card pileCard){
        return ((!(dragCard.getColor().equals(pileCard.getColor())))&&
                (dragCard.getNumber() == pileCard.getNumber() - 1));
    }

    private void rePositionCards(int stackIndex){
        List<Card> stack = board.get(stackIndex);
        for (int iStack = 0 ; iStack < stack.size() ; iStack++) {
            Card card = stack.get(iStack);
            int x = (int) (((stackIndex + 1.0) * (screenWidth / 8.0)) - (cardWidth / 2.0));
            int y = (int) (((screenHeight / 4.0)) + (iStack * (cardHeight / 5.0)));
            card.setPosition(new Point(x, y));
        }
    }




    //Tap event methods
    public void handleTapEvent(Point tap, KlondikeGameStats stats, KlondikeGameView view, MovementAnimator animator, UndoHandler undoHandler){
        //Checks if drawStack was tapped
        Point drawStackPosn = placeHolderPosn.get(DRAW_STACK_POSN);
        if ((tap.x >= drawStackPosn.x)&&(tap.x <= drawStackPosn.x + cardWidth)&&
                (tap.y >= drawStackPosn.y)&&(tap.y <= drawStackPosn.y + cardHeight)){
            handleDrawStackTap();
            stats.addMove();
        }
        //Checks if activeStack was tapped
        else if ((activeStack.size() > 0)&&(activeStack.get(0).isWithinBounds(tap))){
            handleActiveDeckTap(stats, view, animator);
        }
        //Checks if card from board was tapped
        else{
            handleBoardTap(tap, stats, view, animator);
        }
        System.out.println("Tap event complete");
    }


    private void handleDrawStackTap() {
        //If drawStack has cards
        if (drawStack.size() > 0){
            //Clear out activeStack
            if (activeStack.size() > 0){
                for (int i = activeStack.size() - 1 ; i >= 0 ; i--){
                    Card card = activeStack.get(i);
                    wasteStack.add(card);
                }
                activeStack.clear();
            }
            for (int i = Math.min(2, drawStack.size() - 1) ; i >= 0  ; i--){
                Card card = drawStack.get(i);
                card.setPosition(activeStackPosn.get(i));
                activeStack.add(card);
                drawStack.remove(i);
                }
            }
        //If drawStack has no cards
        else if (drawStack.size() == 0){
            if (activeStack.size() > 0){
                for (int i = activeStack.size() - 1 ; i >= 0 ; i--){
                    Card card = activeStack.get(i);
                    wasteStack.add(card);
                }
                activeStack.clear();
            }
            for (Card card: wasteStack){
                drawStack.add(card);
            }
            wasteStack.clear();
        }
        activeStackCleared = false;
    }

    private void handleActiveDeckTap(KlondikeGameStats stats, KlondikeGameView view, MovementAnimator animator) {
        //Checks if first card can be placed on ace stack
        Card card = activeStack.get(0);
        boolean placed = attemptPlaceOnAceStack(card, view, animator);
        if (placed){
            activeStack.remove(0);
            activeStackOutOfCards();
            stats.addMove();
        }
    }

    private void handleBoardTap(Point tap, KlondikeGameStats stats, KlondikeGameView view, MovementAnimator animator){
        //Checks if first card on a stack can be placed on an ace stack
        for (List<Card> stack: board){
            if (stack.size() > 0){
                Card card = stack.get(stack.size() - 1);
                if (card.isWithinBounds(tap)){
                    boolean placed = attemptPlaceOnAceStack(card, view, animator);
                    if (placed){
                        stack.remove(card);
                        if (stack.size() > 0){
                            stack.get(stack.size() - 1).setFaceUp(true);
                        }
                        stats.addMove();
                        return;
                    }
                }
            }
        }
    }

    private void activeStackOutOfCards(){
        if ((activeStack.size() == 0)&&(wasteStack.size() > 0)){
            Card card = wasteStack.get(wasteStack.size() - 1);
            card.setPosition(activeStackPosn.get(0));
            activeStack.add(card);
            wasteStack.remove(wasteStack.size() - 1);
            activeStackCleared = true;
        }
    }

    private boolean attemptPlaceOnAceStack(Card card, KlondikeGameView view, MovementAnimator animator){
        for (int i = 0 ; i < aceStacks.size() ; i++){
            List<Card> stack = aceStacks.get(i);
            if ((stack.size() == 0)&&(card.getNumber() == 1)){
                /*card.setPosition(aceStacksPosn.get(aceStacks.indexOf(stack)));
                stack.add(card);*/
                animator.addAnimatedCard(card);
                animator.addTargetPosn(aceStacksPosn.get(i));;
                animator.startAnimation("aceStack", i, this);
                return true;
            }
            else if ((stack.size() > 0)&&(card.getSuit().equals(stack.get(0).getSuit()))&&
                    (card.getNumber() == stack.get(stack.size() - 1).getNumber() + 1)){
                /*card.setPosition(aceStacksPosn.get(aceStacks.indexOf(stack)));
                stack.add(card);*/
                animator.addAnimatedCard(card);
                animator.addTargetPosn(aceStacksPosn.get(i));
                animator.startAnimation("aceStack", i, this);
                view.checkIfWon();
                return true;
            }
        }
        return false;
    }

    public void placeCardsAfterAnimation(List<Card> cards, String location, int stackIndex){
        if (location.equals("board")){
            List<Card> stack = board.get(stackIndex);
            for (Card card : cards){
                stack.add(card);
            }
            System.out.println("Pile size is " + stack.size());
        }
        else if (location.equals("activeStack")){
            activeStack.add(0, cards.get(0));
        }
        else if (location.equals("aceStack")){
            aceStacks.get(stackIndex).add(cards.get(0));
        }
    }


    public void drawBoard(Canvas canvas, Drag drag){
        for (int iAce = 0 ; iAce < 4 ; iAce++){
            Point posn = aceStacksPosn.get(iAce);
            canvas.drawBitmap(images.getAcePlaceholder(), posn.x, posn.y, null);
        }
        for (int iPH = 0 ; iPH < 8 ; iPH++){
            Point posn = placeHolderPosn.get(iPH);
            canvas.drawBitmap(images.getGenPlaceholder(), posn.x, posn.y, null);
        }
        if (drawStack.size() > 0){
            Point posn = placeHolderPosn.get(DRAW_STACK_POSN);
            canvas.drawBitmap(images.getCardImage(52), posn.x, posn.y, null);
        }
        //To prevent errors between thread and tap events, we use a try here
        try{
            if (activeStack.size() > 0){
                for (int i = activeStack.size() - 1 ; i >= 0 ; i--){
                    Card card = activeStack.get(i);
                    canvas.drawBitmap(images.getCardImage(card.getImdex()), card.getXPos(), card.getYPos(), null);
                }
            }
        }
        catch (IndexOutOfBoundsException e){
            System.out.println("Confliction caused by tap event. Active deck skipped in draw method.");
        }
        if (drag.isDragInProgress()&&(drag.getoLocation().equals("activeStack"))&&(activeStack.size() == 0)&&(wasteStack.size() > 0)){
            Card card = wasteStack.get(wasteStack.size() - 1);
            Point pos = activeStackPosn.get(0);
            canvas.drawBitmap(images.getCardImage(card.getImdex()), pos.x, pos.y, null);
        }
        for (List<Card> stack: aceStacks){
            if (stack.size() > 0){
                Card card = stack.get(stack.size() - 1);
                canvas.drawBitmap(images.getCardImage(card.getImdex()), card.getXPos(), card.getYPos(), null);
            }
        }
        for (int iBoard = 0; iBoard < board.size(); iBoard++) {
            List<Card> pile = board.get(iBoard);
            for (int iPile = 0; iPile < pile.size(); iPile++) {
                Card card = pile.get(iPile);
                if (card.isFaceUp()) {
                    canvas.drawBitmap(images.getCardImage(card.getImdex()), card.getXPos(), card.getYPos(), null);
                }else {
                    canvas.drawBitmap(images.getCardImage(52), card.getXPos(), card.getYPos(), null);
                }
            }
        }
    }


    public LinkedList<LinkedList<LinkedList<Card>>> saveBoardState(LinkedList<LinkedList<LinkedList<Card>>> list){
        LinkedList<LinkedList<Card>> decks = new LinkedList<>();
        decks.add(drawStack);
        decks.add(activeStack);
        decks.add(wasteStack);
        list.add(decks);
        list.add(aceStacks);
        list.add(board);
        return list;
    }

    public void restoreBoard(LinkedList<LinkedList<LinkedList<Card>>> list){
        drawStack = list.get(0).get(0);
        activeStack = list.get(0).get(1);
        wasteStack = list.get(0).get(2);
        aceStacks = list.get(1);
        board = list.get(2);
    }

    public void onPauseReturnDragCards(Drag drag){
        if (drag.getoLocation().equals("activeStack")){
            Card card = drag.getDragCards().get(0);
            card.setPosition(drag.getCardsInitPosns().get(0));
            activeStack.add(drag.getoIndex(), card);
        }
        else if (drag.getoLocation().equals("aceStack")){
            Card card = drag.getDragCards().get(0);
            card.setPosition(drag.getCardsInitPosns().get(0));
            aceStacks.get(drag.getoStackIndex()).add(card);
        }
        else if (drag.getoLocation().equals("board")){
            for (int i = 0 ; i < drag.getDragCards().size() ; i++){
                Card card = drag.getDragCards().get(i);
                card.setPosition(drag.getCardsInitPosns().get(i));
                board.get(drag.getoStackIndex()).add(card);
            }
        }
    }
}
