package com.app.thomas.solitaireplus;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class KlondikeGameView extends SurfaceView implements SurfaceHolder.Callback{

    private KlondikeThread gameThread;
    private Klondike game;
    private KlondikeGameBoard gameBoard;
    private KlondikeGameStats gameStats;
    private StatsHandler statsHandler;
    private MovementAnimator movementAnimator;
    private Drag drag;
    private UndoHandler undoHandler;
    private int screenWidth;
    private int screenHeight;
    private ImageHandler images;
    private boolean gamePaused;
    private boolean createSavedGame;


    public KlondikeGameView(Context context, Klondike game, int screenHeight, int screenWidth, StatsHandler statsHandler, KlondikeGameStats gameStats) {
        super(context);
        getHolder().addCallback(this);
        this.game = game;
        this.gameStats = gameStats;
        this.statsHandler = statsHandler;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.movementAnimator = new MovementAnimator();
        this.gameThread = new KlondikeThread(getHolder(), this, movementAnimator);
        this.images = new ImageHandler(this.screenHeight, this.screenWidth, "klondike", context);
        this.drag = new Drag();
        this.undoHandler = new UndoHandler();
        this.gameBoard = new KlondikeGameBoard(this.images, screenWidth, screenHeight);
        this.gamePaused = false;
        this.gameBoard.initialize();
        this.createSavedGame = true;
    }

    public boolean isGamePaused(){
        return gamePaused;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if ((!gamePaused)&&(!movementAnimator.isAnimationInProgress())){
            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    int xDown = (int)event.getX();
                    int yDown = (int)event.getY();
                    gameBoard.handleInitialDrag(new Point(xDown, yDown), drag);
                    return true;
                case MotionEvent.ACTION_MOVE:
                    int xMove= (int)event.getX();
                    int yMove = (int)event.getY();
                    gameBoard.handleDragMove(new Point(xMove, yMove), drag);
                    return true;
                case MotionEvent.ACTION_UP:
                    int xUp = (int)event.getX();
                    int yUp = (int)event.getY();
                    System.out.println("Number of cards dragged: " + drag.getDragCards().size());
                    if (((drag.isPossibleDrag())&&(!drag.isDragInProgress()))||(!drag.isPossibleDrag())){
                        drag.resetDrag();
                        gameBoard.handleTapEvent(new Point(xUp, yUp), gameStats, this, movementAnimator, undoHandler);
                    }
                    else if (drag.isDragInProgress()){
                        gameBoard.handleDragRelease(new Point(xUp, yUp), drag, gameStats, this, movementAnimator, undoHandler);
                        drag.resetDrag();
                    }
                    return true;
            }
        }
        return true;
    }

    public void Draw(Canvas canvas){
        if (canvas != null){
            canvas.drawColor(Color.BLACK);
            canvas.drawBitmap(images.getBackground(), 0, 0, null);
            gameBoard.drawBoard(canvas, drag);
            drag.drawDragCards(canvas, images);
            movementAnimator.drawCards(canvas, images);
        }
    }

    public void checkIfWon(){
        if (gameBoard.checkIfAceStacksFull()) {
            System.out.println("Game won");
            gamePaused = true;
            gameThread.setRunning(false);
            createSavedGame = false;
            gameStats.setGameWon(true);
            gameStats.addTime(System.currentTimeMillis());
            game.statsHandler.writeToFile(getContext(), gameStats);
            game.statsHandler.compileWinScreen(game.winScreen, game.getApplicationContext(), gameStats);
            game.winScreen.setVisibility(VISIBLE);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        System.out.println("Surface created");
        gameThread.setRunning(true);
        gameThread.start();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try{
                gameThread.join();
                retry=false;
                System.out.println("Surface destroyed.");
            } catch (InterruptedException e){
                System.out.println("Error destroying thread...attempting to retry...");
            }
        }
    }

    public void setGameThreadRunningStatus(boolean running){
        gameThread.setRunning(running);
    }

    public void setGamePaused(boolean paused){
        gamePaused = paused;
    }

    public KlondikeGameBoard getGameBoard() {
        return gameBoard;
    }

    public Drag getDrag(){
        return drag;
    }

    public void updateStats(KlondikeGameStats stats){
        this.gameStats = stats;
    }

    public boolean createSaveGame(){
        return createSavedGame;
    }

    public void setCreateSavedGame(boolean save){
        createSavedGame = save;
    }
}
