package com.app.thomas.solitaireplus;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class Klondike extends Activity{

    private KlondikeGameView gameView;
    public StatsHandler statsHandler;
    private KlondikeGameStats gameStats;
    private RelativeLayout mainLayout;
    private AdView adView;
    private ImageButton optionsButton;
    private View optionsMenu;
    private View statsScreen;
    public View winScreen;
    public boolean createSavedGame;
    private DisplayMetrics dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createSavedGame = true;
        setContentView(R.layout.klondike);
        mainLayout = (RelativeLayout) findViewById(R.id.main_klondike_layout);
        dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);

        this.statsHandler = new StatsHandler();
        this.gameStats = new KlondikeGameStats(System.currentTimeMillis());


        //Creation of views and widgets
        gameView = new KlondikeGameView(getApplicationContext(), this, dm.heightPixels, dm.widthPixels, statsHandler, gameStats);
        mainLayout.addView(gameView);

        Typeface tf = Typeface.createFromAsset(getAssets(), "bodinxt.ttf");
        Typeface buttontf = Typeface.createFromAsset(getAssets(), "bodinxt.ttf");

        //Creates and handles adView
        adView = new AdView(getApplicationContext());
        adView.setAdSize(AdSize.BANNER);
        //From the ad I created from my Admob account
        adView.setAdUnitId("ca-app-pub-6752164172261392/4623507267");
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        //This allows ads to be displayed on emulators
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        //This allows ads to be tested on my phone
        //adRequestBuilder.addTestDevice("28F7E015672B0E8A8EA4E3D7D00DBFC7");
        mainLayout.addView(adView);
        adView.setId(R.id.ad_view);
        RelativeLayout.LayoutParams adParameters = new RelativeLayout.LayoutParams(adView.getLayoutParams().width, adView.getLayoutParams().height);
        adParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
        adView.setLayoutParams(adParameters);
        adView.loadAd(adRequestBuilder.build());

        optionsButton = new ImageButton(getApplicationContext());
        optionsButton.setBackgroundResource(R.drawable.options_button);
        mainLayout.addView(optionsButton);
        RelativeLayout.LayoutParams optionButtonParams = new RelativeLayout.LayoutParams(100, 100);
        optionButtonParams.addRule(RelativeLayout.ABOVE, adView.getId());
        optionButtonParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        optionButtonParams.setMargins(0, 0, 0, 30);
        optionsButton.setLayoutParams(optionButtonParams);

        optionsButton.setOnClickListener(optionsButtonClick);

        LayoutInflater optionsInflater = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        optionsMenu = optionsInflater.inflate(R.layout.options_menu, null, false);
        mainLayout.addView(optionsMenu);
        TextView optionsMenuText = (TextView) findViewById(R.id.textViewOptionsTitle);
        optionsMenuText.setTypeface(tf);
        optionsMenu.setVisibility(View.GONE);

        Button mainMenuButton = (Button) findViewById(R.id.buttonMainMenu);
        mainMenuButton.setOnClickListener(mainMenuButtonListener);
        mainMenuButton.setTypeface(buttontf);
        mainMenuButton.setTransformationMethod(null);

        Button newGameButton = (Button) findViewById(R.id.buttonNewGame);
        newGameButton.setOnClickListener(newGameButtonListener);
        newGameButton.setTypeface(buttontf);
        newGameButton.setTransformationMethod(null);

        Button resumeButton = (Button) findViewById(R.id.buttonResume);
        resumeButton.setOnClickListener(resumeButtonListener);
        resumeButton.setTypeface(buttontf);
        resumeButton.setTransformationMethod(null);

        Button statsButton = (Button) findViewById(R.id.buttonStats);
        statsButton.setOnClickListener(statsButtonListener);
        statsButton.setTypeface(buttontf);
        statsButton.setTransformationMethod(null);

        LayoutInflater winScreenInflator = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        winScreen = winScreenInflator.inflate(R.layout.win_screen, null, false);
        mainLayout.addView(winScreen);
        TextView winScreenText = (TextView) findViewById(R.id.youWin);
        winScreenText.setTypeface(tf);
        winScreen.setVisibility(View.GONE);

        Button playAgainButton = (Button) findViewById(R.id.buttonPlayAgain);
        playAgainButton.setOnClickListener(playAgainButtonListener);
        playAgainButton.setTypeface(buttontf);
        playAgainButton.setTransformationMethod(null);

        Button mainMenuWinScreenButton = (Button) findViewById(R.id.buttonMainMenuWinScreen);
        mainMenuWinScreenButton.setOnClickListener(mainMenuWinScreenButtonListener);
        mainMenuWinScreenButton.setTypeface(buttontf);
        mainMenuWinScreenButton.setTransformationMethod(null);

        LayoutInflater statsScreenInflator = (LayoutInflater) getApplicationContext().getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
        statsScreen = statsScreenInflator.inflate(R.layout.stats_screen, null, false);
        mainLayout.addView(statsScreen);
        TextView statsScreenText = (TextView) findViewById(R.id.statisticsTitle);
        statsScreenText.setTypeface(tf);
        statsScreen.setVisibility(View.GONE);

        Button closeStatsScreenButton = (Button) findViewById(R.id.buttonCloseStatsScreen);
        closeStatsScreenButton.setOnClickListener(closeStatsButtonListener);
        closeStatsScreenButton.setTypeface(buttontf);
        closeStatsScreenButton.setTransformationMethod(null);

        Button resetStatsButton = (Button) findViewById(R.id.buttonResetStats);
        resetStatsButton.setOnClickListener(resetStatsButtonListener);
        resetStatsButton.setTypeface(buttontf);
        resetStatsButton.setTransformationMethod(null);
    }

    OnClickListener optionsButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!gameView.isGamePaused()){
                gameView.setGamePaused(true);
                if (gameView.getDrag().getDragCards().size() > 0){
                    gameView.getGameBoard().onPauseReturnDragCards(gameView.getDrag());
                }
                optionsMenu.setVisibility(View.VISIBLE);
            }
        }
    };

    OnClickListener mainMenuButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            gameView.setGameThreadRunningStatus(false);
            Klondike.this.finish();
        }
    };

    OnClickListener newGameButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            gameView.setGameThreadRunningStatus(false);
            gameView.setCreateSavedGame(false);
            gameStats.addTime(System.currentTimeMillis());
            statsHandler.writeToFile(getApplicationContext(), gameStats);
            Intent myIntent = new Intent(getApplicationContext(), Klondike.class);
            Klondike.this.finish();
            startActivity(myIntent);
        }
    };

    OnClickListener resumeButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            optionsMenu.setVisibility(View.GONE);
            gameView.setGamePaused(false);
        }
    };

    OnClickListener statsButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            optionsMenu.setVisibility(View.GONE);
            statsHandler.compileStatsScreen(statsScreen, getApplicationContext());
            statsScreen.setVisibility(View.VISIBLE);
        }
    };

    OnClickListener closeStatsButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            statsScreen.setVisibility(View.GONE);
            gameView.setGamePaused(false);
        }
    };

    OnClickListener resetStatsButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            statsHandler.resetStatistics(getApplicationContext());
            statsHandler.compileStatsScreen(statsScreen, getApplicationContext());
        }
    };

    OnClickListener playAgainButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            gameView.setGameThreadRunningStatus(false);
            createSavedGame = false;
            Intent myIntent = new Intent(getApplicationContext(), Klondike.class);
            Klondike.this.finish();
            startActivity(myIntent);
        }
    };

    OnClickListener mainMenuWinScreenButtonListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            gameView.setGameThreadRunningStatus(false);
            createSavedGame = false;
            Klondike.this.finish();
        }
    };



    //Activity Life Cycle:

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("OnStart was called for Klondike");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("OnResume was called for Klondike");
        if (KlondikeSaveHandler.checkIfSavedStateExists(getApplicationContext())){
            gameStats = KlondikeSaveHandler.restoreSavedState(getApplicationContext(), gameView.getGameBoard());
            gameView.updateStats(gameStats);
        }
        adView.resume();
        gameStats.modifiyStartTime(System.currentTimeMillis());
        gameView.setGameThreadRunningStatus(true);
        gameView.setGamePaused(false);

    }

    @Override
    protected void onPause() {
        System.out.println("OnPause was called for Klondike");
        adView.pause();
        createSavedGame = gameView.createSaveGame();
        if (gameView.getDrag().getDragCards().size() > 0){
            gameView.getGameBoard().onPauseReturnDragCards(gameView.getDrag());
        }
        gameStats.addTime(System.currentTimeMillis());
        if (createSavedGame){
            KlondikeSaveHandler.writeStateToFile(getApplicationContext(), gameStats, gameView.getGameBoard());
            System.out.println("SaveGame File written");
        }
        else {
            KlondikeSaveHandler.deleteSavedGame(getApplicationContext());
            System.out.println("Game not eligible for saving, saved state deleted.");
        }
        gameView.setGamePaused(true);
        gameView.setGameThreadRunningStatus(false);
        Klondike.this.finish();
        if (isFinishing()){
            System.out.println("Closing game");
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("OnStop was called for Klondike");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("OnRestart was called for Klondike");
    }

    @Override
    protected void onDestroy() {
        adView.destroy();
        super.onDestroy();
        System.out.println("OnDestroy was called for Klondike");
    }



}
