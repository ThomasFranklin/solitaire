package com.app.thomas.solitaireplus;


import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class KlondikeThread extends Thread{


    private boolean running;
    private SurfaceHolder holder;
    private KlondikeGameView view;
    private MovementAnimator movementAnimator;


    public KlondikeThread(SurfaceHolder holder, KlondikeGameView view, MovementAnimator movementAnimator) {
        this.running = false;
        this.holder = holder;
        this.view = view;
        this.movementAnimator = movementAnimator;
    }

    public void setRunning(boolean isRunning){
        this.running = isRunning;
    }

    @Override
    public void run() {
        Canvas canvas;

        while (running) {
            canvas = null;
            try{
                canvas = this.holder.lockCanvas();
                synchronized (holder) {
                    view.Draw(canvas);
                }
            }
            finally{
                if (canvas != null)
                    holder.unlockCanvasAndPost(canvas);
            }
        }
    }
}
