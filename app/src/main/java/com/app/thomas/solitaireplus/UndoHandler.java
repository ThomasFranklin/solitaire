package com.app.thomas.solitaireplus;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class UndoHandler implements Serializable{

    private static final int UNDO_LIMIT = 50;
    private List<UndoState> undoStack;

    public UndoHandler(){
        this.undoStack = new LinkedList<>();
    }

    public void createUndoState(List<Card> cards, List<Point> initialPosns, String fLocation, int fStackIndex, int fIndex,  String oLocation, int oStackIndex, boolean activeStackCleared){
        undoStack.add(0, new UndoState(cards, initialPosns, fLocation, fStackIndex, fIndex, oLocation, oStackIndex, activeStackCleared));
        while (undoStack.size() > UNDO_LIMIT){
            undoStack.remove(UNDO_LIMIT);
        }
        System.out.println("Undo added to undo stack. Size of stack is: " + undoStack.size());
    }

    public UndoState retrieveFirstUndoState(){
        UndoState state = undoStack.get(0);
        undoStack.remove(0);
        return state;
    }


    private class UndoState implements Serializable{

        private List<Card> cards;
        private List<Point> initialPosns;
        private String fLocation;
        private int fStackIndex;
        private int fIndex;
        private String oLocation;
        private int oStackIndex;
        private boolean activeStackCleared;

        public UndoState(List<Card> cards, List<Point> initialPosns, String fLocation, int fStackIndex, int fIndex,  String oLocation, int oStackIndex, boolean activeStackCleared) {
            this.cards = new LinkedList<>();
            for (Card card: cards){
                this.cards.add(card.cloneCard());
            }
            this.initialPosns = new LinkedList<>();
            for (Point point: initialPosns){
                this.initialPosns.add(new Point(point.x, point.y));
            }
            this.initialPosns = initialPosns;
            this.fIndex = fIndex;
            this.fLocation = fLocation;
            this.fStackIndex = fStackIndex;
            this.oLocation = oLocation;
            this.oStackIndex = oStackIndex;
            this.activeStackCleared = activeStackCleared;
        }

        public List<Card> getCards() {
            return cards;
        }

        public int getfIndex() {
            return fIndex;
        }

        public String getfLocation() {
            return fLocation;
        }

        public int getfStackIndex() {
            return fStackIndex;
        }

        public List<Point> getInitialPosns() {
            return initialPosns;
        }

        public String getoLocation() {
            return oLocation;
        }

        public int getoStackIndex() {
            return oStackIndex;
        }

        public boolean isActiveStackCleared() {
            return activeStackCleared;
        }
    }
}
