package com.app.thomas.solitaireplus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainMenu extends Activity {

    private StatsHandler statsHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        System.out.println("onCreate for main menu was called");

        statsHandler = new StatsHandler();
    }

    public void selectResume(View view) {
        Intent intent = new Intent(this, Klondike.class);
        startActivity(intent);
    }

    public void selectNewGame(View view){
        KlondikeGameStats gameStats = KlondikeSaveHandler.getSavedGameStats(getApplicationContext());
        if (gameStats != null){
            statsHandler.writeToFile(getApplicationContext(), gameStats);
        }
        KlondikeSaveHandler.deleteSavedGame(getApplicationContext());
        Intent intent = new Intent(this, Klondike.class);
        startActivity(intent);
    }

    public void selectAbout(View view){
        View menu = findViewById(R.id.mainMenuButtons);
        menu.setVisibility(View.GONE);
        View aboutScreen = findViewById(R.id.appInfoView);
        aboutScreen.setVisibility(View.VISIBLE);
    }

    public void selectAboutScreenBack(View view){
        View aboutScreen = findViewById(R.id.appInfoView);
        aboutScreen.setVisibility(View.GONE);
        View menu = findViewById(R.id.mainMenuButtons);
        menu.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("onDestroy for main menu was called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("onPause for main menu was called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("onRestart for main menu was called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume for main menu was called");

        Typeface buttontf = Typeface.createFromAsset(getAssets(), "bodinxt.ttf");

        Button resumeButton = (Button) findViewById(R.id.mainMenuResume);
        resumeButton.setTypeface(buttontf);
        resumeButton.setTransformationMethod(null);
        resumeButton.setVisibility(View.VISIBLE);
        if (!KlondikeSaveHandler.checkIfSavedStateExists(getApplicationContext())){
            resumeButton.setVisibility(View.GONE);
        }

        Button newGameButton = (Button) findViewById(R.id.mainMenuNewGame);
        newGameButton.setTypeface(buttontf);
        newGameButton.setTransformationMethod(null);

        Button aboutButton = (Button) findViewById(R.id.mainMenuAbout);
        aboutButton.setTypeface(buttontf);
        aboutButton.setTransformationMethod(null);

        Button aboutBackButton = (Button) findViewById(R.id.aboutBackButton);
        aboutBackButton.setTypeface(buttontf);
        aboutBackButton.setTransformationMethod(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("onStart for main menu was called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("onStop for main menu was called");
    }
}
